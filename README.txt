i18n Localized Variables

Author:
DesignEnd, Andrzej Kluczny
http://www.DesignEnd.net/

-- DESCRIPTION --

Extends i18n module to let Drupal utilize localized variables for each enabled language
without modifying settings.php file.

It allows to automatically use all available variables that are registered in Drupal variable table
and loaded to $conf array at Drupal boot time.

-- INSTALLATION --

Nothing special here.

-- CONFIGURATION --

'i18n localized variables' configuration is available on
Site configuration -> Languages -> Multilingual system -> i18n localized variables.
You can include modules which variables should be localized either select more granular option
using the second select list, to check only required variables without checking all variables related
to several modules.

In addition, after submitting the "Restore defaults" button, predefined variables are loaded. 
Custom predefined variables are:

date_first_day
date_default_timezone
date_format_long
date_format_medium
date_format_short
date_format_long_custom
date_format_medium_custom
date_format_short_custom
site_403
site_404
site_footer
site_mission
site_slogan
site_frontpage
site_mail
site_name